##test_stations_level_over_threshold
from floodsystem.station import MonitoringStation 
from floodsystem.flood import stations_level_over_threshold
def test_stations_level_over_threshold():
    #create dummy list of monitoring objects
    x = MonitoringStation("a","b", "c", (-1.0,11.0,6), (0.0,8.0), "f", "g")
    y = MonitoringStation("h", "i", "j", (7.0,4.3), (1.0,5.0), "f", "n")
    z = MonitoringStation("o", "p", "q", (4.7,3.8),(-2.3,9.8), "t", "u")
    v = MonitoringStation("o", "p", "q", (0.0,6.0),(2.3,2.0), "y", "u")
    u = MonitoringStation("o", "p", "l", (0.0,6.0),(1.0,2.0), "y", "u")
    x.latest_level = 6
    y.latest_level = 10.0
    z.latest_level = 777.9
    v.latest_level = 2.7
    u.latest_level = None
    w=[x,y,z,v,u]
    #create list of station siwth relative water level over 0.7 
    a=stations_level_over_threshold(w,0.7)
    #check outcome is as expected
    assert len(a)== 2
    assert type(a) == list 
    assert type(a[0]) == tuple
    assert type(a[1]) == tuple
    b=a[0]
    c=a[1]
    assert b[0] == "c" or b[0] == "j"
    assert c[0] == "c" or c[0] == "j"
    assert b[1]== 0.75 or b[1] == 2.25
    assert c[1]== 0.75 or c[1] == 2.25

