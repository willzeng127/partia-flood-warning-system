from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations
def run():
    #create list of monitoring station objects 
    stations=build_station_list()
    #get list of inconsistent data stations
    a=inconsistent_typical_range_stations(stations)
    #sort alphabetically
    a.sort()
    #print result
    print(a)
if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()