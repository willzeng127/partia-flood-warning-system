##Task2B
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list, update_water_levels

def run():
    # Build list of stations
    stations = build_station_list()

    # Update latest level data for all stations
    update_water_levels(stations)
    # create list of tuples of at risk stations
    a=(stations_level_over_threshold(stations,0.8))
    
    #iterate through and print
    for i in a:
        print (i)


if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
