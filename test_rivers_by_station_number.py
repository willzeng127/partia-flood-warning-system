from floodsystem.geo import rivers_by_station_number
from floodsystem.station import MonitoringStation 
def test_rivers_by_station_number():
    #create dummy list of monitoring station objects
    x = MonitoringStation("a","b", "c", (-1.0,-1.0), (1.0,1.0), "f", "g")
    y = MonitoringStation("h", "i", "j", (2.3,-9.8), (2,0,4.0), "f", "n")
    z = MonitoringStation("o", "p", "q", (0.0,6.0),(-2.3,9.8), "t", "u")
    v = MonitoringStation("o", "p", "q", (0.0,6.0),(-2.3,9.8), "y", "u")
    #create list of tuple with river with most stations and how many stations
    w=rivers_by_station_number([x,y,z,v],1)
    #check results are correct
    assert len(w)==1
    assert ("f",2) in w
    #create list of tuple with river with two most stations and how many stations
    w=rivers_by_station_number([x,y,z,v],2)
    assert len(w)==3
    assert ("f",2) in w
    assert ("t",1) in w
    assert ("y",1) in w
    #check results, note there is three tuples in the list as there are two rivers with 1 station each

