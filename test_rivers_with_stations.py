from floodsystem.station import MonitoringStation
from floodsystem.geo import rivers_with_station
def test_river_with_stations():
    #create dummy list of monitoring station objects
    x = MonitoringStation("a","b", "c", (-1.0,-1.0), (1.0,1.0), "f", "g")
    y = MonitoringStation("h", "i", "j", (2.3,-9.8), (2,0,4.0), "f", "n")
    z = MonitoringStation("o", "p", "q", (0.0,6.0),(-2.3,9.8), "t", "u")
    w=[x,y,z]
    # create list with all rivers 
    v=rivers_with_station(w)
    #check result contains two elements and they are the correct elements 
    assert len(v)==2
    assert "f" in v
    assert "t" in v
