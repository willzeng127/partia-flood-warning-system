from floodsystem.utils import sorted_by_key
from haversine import haversine, Unit
from floodsystem.stationdata import build_station_list

def stations_by_distance(stations, p):
    #create empty lists for location and names
    location = list()  
    names = list() 
    town = list() 
    for station in stations:
        distances = haversine(station.coord, p)
        location.append(distances)
    for station in stations:
        names.append(station.name)
    for station in stations:
        town.append(station.town)
    #sort by distance
    return sorted_by_key(list(zip(names, town, location)), 2)

print((stations_by_distance(build_station_list(), (52.2053, 0.1218))[:10]))