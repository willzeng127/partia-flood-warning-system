from floodsystem.station import MonitoringStation
from floodsystem.geo import stations_by_river
def test_stations_by_river():
    #create dummy list of monitoring station objects
    x = MonitoringStation("a","b", "c", (-1.0,-1.0), (1.0,1.0), "f", "g")
    y = MonitoringStation("h", "i", "j", (2.3,-9.8), (2,0,4.0), "f", "n")
    z = MonitoringStation("o", "p", "q", (0.0,6.0),(-2.3,9.8), "t", "u")
    w=[x,y,z]
    #create dictionary of rivers to staions
    v=stations_by_river(w)
    #check correct elements are present and no others 
    assert "c" in v["f"]
    assert "j" in v ["f"]
    assert len(v["f"])==2
    assert "q" in v["t"]
    assert len(v["t"])==1