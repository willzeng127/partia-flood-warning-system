#Task 1D
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
def run():
    #create list of monitoring staion objects
    stations= build_station_list()
    #create list of rivers with stations
    rivers=rivers_with_station(stations)
    #sort rivers alphabetically
    a=sorted(rivers)
    #print first ten
    print (a[0:10])
    #create dictionary of each river and all its staions
    stationsbyriver=stations_by_river(stations)
    #print river name and stations sorted alphabetically for three rivers
    a=stationsbyriver['River Aire']
    a.sort()
    print(a)
    a=stationsbyriver['River Cam']
    a.sort()
    print(a)
    a=stationsbyriver['River Thames']
    a.sort()
    print(a)



if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()
        