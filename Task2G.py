
from datetime import datetime, timedelta, date
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_level_over_threshold


risky = []
stations = build_station_list()
update_water_levels(stations)

a = stations_level_over_threshold(stations, 1.5)
b = [item[0] for item in a]

for i in b:
    station_name = i

    # Find station
    station_ = None
    for station in stations:
        if station.name == station_name:
            station_ = station
    
            dates, levels = fetch_measure_levels(
                station_.measure_id, dt=timedelta(days=2))
            if len(levels) > 0:
                if (levels[-1] - levels[0]) > 0.3:
                    risky.append("{} : Severe".format(station.name))
                if (levels[-1] - levels[0]) > 0 and (levels[-1] - levels[0]) < 0.3:
                    risky.append("{} : High".format(station.name))
                if (levels[-1] - levels[0]) > -0.3 and (levels[-1] - levels[0]) < 0:
                    risky.append("{} : Moderate".format(station.name))
                if (levels[-1] - levels[0]) < -0.3:
                    risky.append("{} : Low".format(station.name))  
print(risky)