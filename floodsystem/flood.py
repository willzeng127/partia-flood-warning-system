#floods
from .station import MonitoringStation

def stations_level_over_threshold(stations, tol):
    #create empty set
    a=[]
    #iterate through stations
    for i in stations:
        #check relative water level exists ie all data is valid
        if i.relative_water_level() != None:
            #check relative water level is bigger than the tolerance 
            if i.relative_water_level() >tol:
                #check relative watre level is less than 2.5 as this suggests incorrect data
                if i.relative_water_level() < 2.5:
                    #if data is valid and gretaer than tol add a tuple with name and relative water level to a list
                    a.append((i.name,i.relative_water_level()))
    a.sort(key = lambda x: x[1], reverse=True)
    #return list
    return a

def stations_highest_rel_level(stations, N):
    #create list of stations over 0, ie almost all stations ordered 
    a= stations_level_over_threshold(stations,0)
    #create empty list
    b=[]
    #iterste through first n items and add them to a new list
    for i in range(N):
        b.append(a[i])
    #return new list 
    return b
