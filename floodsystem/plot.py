import matplotlib.pyplot as plt
from datetime import datetime, timedelta, date
#from floodsystem.stationdata import build_station_list
#from floodsystem.datafetcher import fetch_measure_levels
import numpy as np
import matplotlib.pyplot as plt
import matplotlib


def plot_water_levels(station, dates, levels):

    plt.axhline(y=station.typical_range[0], color='g', linestyle='-')
    plt.axhline(y=station.typical_range[1], color='r', linestyle='-')
    plt.plot(dates, levels)
    plt.xlim(datetime.now() - timedelta(days=10), datetime.now())
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    plt.tight_layout()  # This makes sure plot does not cut off date labels

    return (plt.show())

def plot_water_level_with_fit(station, dates, levels, p):
    
    x = matplotlib.dates.date2num(dates)
    y = levels

    # Find coefficients of best-fit polynomial f(x) of degree 4
    p_coeff = np.polyfit(x, y, p)

    # Convert coefficient into a polynomial that can be evaluated,
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)
    
    # Plot polynomial fit at 30 points along interval
    x1 = np.linspace(x[0], x[-1], 30)
    plt.plot(x1, poly(x1))


    plt.axhline(y=station.typical_range[0], color='g', linestyle='-')
    plt.axhline(y=station.typical_range[1], color='r', linestyle='-')
    plt.plot(dates, levels)
    plt.xlim(datetime.now() - timedelta(days=2), datetime.now())
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    plt.tight_layout()  # This makes sure plot does not cut off date labels

    return (plt.show())