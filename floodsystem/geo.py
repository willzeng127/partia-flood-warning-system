# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.
"""
from floodsystem.utils import sorted_by_key #noqa

from floodsystem.stationdata import build_station_list

from floodsystem.utils import sorted_by_key
from haversine import haversine, Unit


def stations_by_distance(stations, p):
    #create empty lists for location and names
    location = list()  
    names = list() 
    town = list() 
    for station in stations:
        distances = haversine(station.coord, p)
        location.append(distances)
    for station in stations:
        names.append(station.name)
    for station in stations:
        town.append(station.town)
    #sort by distance
    return sorted_by_key(list(zip(names, town, location)), 2)
    
def stations_within_radius(stations, centre, r):
    inradius = list()
    for station in stations:
        distances = haversine(station.coord, centre)
        if distances < r:
            inradius.append(station.name)
    inradius.sort()
    return inradius

def rivers_with_station(stations):
    #create empty set for the rivers so each river appears once
    rivers=set()
    #iterate through stations adding the river names 
    for i in stations:
        rivers.add(i.river)
    return rivers

def stations_by_river(stations):
    #create empty dictionary for rivers to staions
    riverdict={}
    #create list of rivers 
    rivers=rivers_with_station(stations)
    #iterate through each river 
    for i in rivers:
        #create empty list
        a=[]
        #iterate through all stations checking whetehr each station is on given river, if so add to list 
        for j in stations:
            if i==j.river:
                a.append(j.name)
        #add to dictionary river name and a list of all the stations on it 
        riverdict[i]=a
    return riverdict


def rivers_by_station_number(stations, N):
    #create empty list for rivers and their number of staions
    rivernumbers=[]
    #create dictionary of river to list of stations
    x=stations_by_river(stations)
    #for every item in dictionary,add a tuple with river name and the number of stations to the list 
    for river,stationnom in x.items():
        y=(river,len(stationnom))
        rivernumbers.append(y)
    #sort list in reverse numerical order on second entry in each tuple
    rivernumbers.sort(key = lambda x: x[1], reverse=True)
    #take the first n entries and add them to a list 
    z=rivernumbers[0:N]
    #check if there are rivers with the same number of staions as the last river in the list 
    while rivernumbers[N][1]==rivernumbers[N-1][1]:
        z.append(rivernumbers[N])
        N+=1
        #if statement makes sure n is never larger than the length of the
        # list which occurs if all elements of the original dictionary should be in the final list
        if N==len(rivernumbers):
            return z
    return z
