from haversine import haversine, Unit
from floodsystem.stationdata import build_station_list
stations = build_station_list()
def stations_within_radius(stations, centre, r):
    inradius = list()
    for station in stations:
        distances = haversine(station.coord, centre)
        if distances < r:
            inradius.append(station.name)
    inradius.sort()
    return inradius

print(stations_within_radius(build_station_list(),(52.2053, 0.1218),10))