from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.station import MonitoringStation 
def test_typical_range_consistent():
    #create dummy list of monitoring objects
    x = MonitoringStation("a","b", "c", (-1.0,11.0,6), (1.0,7.0,6.0), "f", "g")
    y = MonitoringStation("h", "i", "j", (7.0,4.3), None, "f", "n")
    z = MonitoringStation("o", "p", "q", (4.7,3.8),(-2.3,9.8), "t", "u")
    v = MonitoringStation("o", "p", "q", (0.0,6.0),(2.3,2.0), "y", "u")
    u = MonitoringStation("o", "p", "l", (0.0,6.0),(True,2.0), "y", "u")
    w=[x,y,z,v,u]
    #create list of inconsistent typical range data stations
    w=inconsistent_typical_range_stations(w)
    #check correct elements are present and no others 
    assert "j" in w
    assert "c" in w
    assert "q" in w
    assert "l" in w
    assert len(w) == 4
    
