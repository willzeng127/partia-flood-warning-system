import matplotlib.pyplot as plt
from datetime import datetime, timedelta, date
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels
from floodsystem.flood import stations_highest_rel_level


stations = build_station_list()
update_water_levels(stations)

a = stations_highest_rel_level(stations,5)
b = [item[0] for item in a]
for i in b:
    station_name = i

    # Find station
    station_cam = None
    for station in stations:
        if station.name == station_name:
            station_cam = station
            break
    if not station_cam:
        print("Station {} could not be found".format(station_name))
    dt = 10
    dates, levels = fetch_measure_levels(
        station_cam.measure_id, dt=timedelta(days=dt))

    plot_water_levels(station, dates, levels)