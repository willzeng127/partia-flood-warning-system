# test_stations_highest_rel_level
from floodsystem.flood import stations_highest_rel_level
from floodsystem.station import MonitoringStation 
def test_stations_highest_rel_level():
    #create dummy list of monitoring objects
    x = MonitoringStation("a","b", "c", (-1.0,11.0,6), (0.0,8.0), "f", "g")
    y = MonitoringStation("h", "i", "j", (7.0,4.3), (1.0,5.0), "f", "n")
    z = MonitoringStation("o", "p", "q", (4.7,3.8),(-2.3,9.8), "t", "u")
    v = MonitoringStation("o", "p", "q", (0.0,6.0),(2.3,2.8), "y", "u")
    u = MonitoringStation("o", "p", "l", (0.0,6.0),(1.0,2.0), "y", "u")
    x.latest_level = 16.0
    y.latest_level = 6.0
    z.latest_level = 777.9
    v.latest_level = 2.4
    u.latest_level = None
    w=[x,y,z,v,u]
    #create list of 2 stations with highest relative water levels
    a=stations_highest_rel_level(w,2)
    #check outcome as expected
    assert len(a)== 2
    assert type(a) == list 
    assert type(a[0]) == tuple
    assert type(a[1]) == tuple
    b=a[0]
    c=a[1]
    assert b[0] == "c" 
    assert c[0] == "j"
    assert b[1]== 2
    assert c[1]== 1.25